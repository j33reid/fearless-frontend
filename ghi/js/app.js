function createCard(title, location, description, pictureUrl, dateStart, dateEnd) {
    return `
        <div class="col">

                <div class="card">
                    <img src="${pictureUrl}" class="card-img-top">
                    <div class="card-body">
                    <h5 class="card-title">${title}</h5>
                    <h6 class="card-subtitle text-secondary">${location}</h6>
                    <p class="card-text">${description}</p>
                    </div>
                    <div class="card-footer">
                    <p class="card-text">${dateStart} - ${dateEnd}</p>
                    </div>
                </div>

        </div>
    `;
  }

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const location = details.conference.location.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const start = new Date(details.conference.starts);
            const dateStart = start.toLocaleDateString();
            const end = new Date(details.conference.ends);
            const dateEnd = end.toLocaleDateString();
            const html = createCard(title, location, description, pictureUrl, dateStart, dateEnd);
            const column = document.querySelector('.row');
            column.innerHTML += html;
          }
        }

      }
    } catch (e) {
        console.error("Embicile, hands off!!")
      // Figure out what to do if an error is raised
    }

  });
