window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');


    const url = 'http://localhost:8000/api/conferences/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();

      for (let conference of data.conferences){
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }
    }


    const formTag = document.getElementById('create-presentation-form');
    formTag.addEventListener('submit', async event => {
      event.preventDefault();
      const formData = new FormData(formTag);
      const json = JSON.stringify(Object.fromEntries(formData));
      const conference_id = Object.fromEntries(formData.conferences.id)

      const presentationUrl = 'http://localhost:8000/api/conferences/${conference_id}/presentations/';
      const fetchConfig = {
        method: "post",
        body: json,
        headers: {
            'Content-Type': 'application/json',
        },
      };
      const responses = await fetch(presentationUrl, fetchConfig);
      if (responses.ok) {
        formTag.reset();
        const newpresentation = await responses.json();
      }
    });

  });
